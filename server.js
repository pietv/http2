const express = require('express');
const mysql = require('mysql');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const db = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'asdf',
    database: 'db'
});

db.connect((err) => {
    if(err){
        throw err;
    }
    console.log('mysql connected');
});



const app = express();
app.use(cors());
app.use(morgan('combined'));

app.use(bodyParser.urlencoded({extended:false}));


const PORT = 8000;
const HOST = '127.0.0.1';



app.post('/Person', (req, res) => {
    let first = req.body.first;
    let last = req.body.last;
    let age = req.body.age;
    if (!first) {
        return res.status(400).send({error: true, message: 'Please provide first'});
    }
    if (!last) {
        return res.status(400).send({error: true, message: 'Please provide last'});
    }
    if (!age) {
        return res.status(400).send({error: true, message: 'Please provide age'});
    }
    let sql = `INSERT INTO registration (first, last, age) VALUES ('${first}', '${last}', '${age}') `;
    let query = db.query(sql, (err, result, fields) => {
        if (err) throw err;
        console.log(result);
        return res.send({error: false, data: result, message: 'New registration has been created successfully.'});
    });
});

app.get('/People', (req, res) => {
    let sql = 'SELECT * FROM registration';
    let query = db.query(sql, (err, results, fields) => {
        if (err) throw err;
        console.log(results);
        return res.send({error: false, data: results, message: 'regs list'});
    });
});

app.get('/Person/:id', (req, res) => {
    let user_id = req.params.id;
    if (!user_id) {
        return res.status(400).send({error: true, message: 'Please provide id'});
    }
    let sql = `SELECT * FROM registration WHERE id = ${user_id}`;
    let query = db.query(sql, (err, result, fields) => {
        if (err) throw err;
        console.log(result);
        return res.send({error: false, data: result[0], message: 'regs list.'});
    });
});

app.put('/Person', (req, res) => {
    let user_id = req.body.id;
    let newFirst = req.body.first;
    let newLast = req.body.last;
    let newAge = req.body.age;
    if (!user_id) {
        return res.status(400).send({error: true, message: 'Please provide id'});
    }
    if (!newFirst) {
        return res.status(400).send({error: true, message: 'Please provide first'});
    }
    if (!newLast) {
        return res.status(400).send({error: true, message: 'Please provide last'});
    }
    if (!newAge) {
        return res.status(400).send({error: true, message: 'Please provide age'});
    }
    
    let sql = `UPDATE registration SET age = ${newAge}, first = '${newFirst}', last = '${newLast}' WHERE id = ${user_id}`;
    let query = db.query(sql, (err, result, fields) => {
        if (err) throw err;
        console.log(result);
        return res.send({error: false, data: result, message: 'reg has been updated successfully'});
    });
});

app.delete('/Person', (req, res) => {
    let user_id = req.body.id;
    if (!user_id) {
        return res.status(400).send({error: true, message: 'Please provide id'});
    }
    let sql = `DELETE FROM registration WHERE id = ${user_id}`;
    let query = db.query(sql, (err, result, fields) => {
        if (err) throw err;
        console.log(result);
        return res.send({error: false, data: result, message: 'reg has been deleted successfully.'});
    });
});

app.listen(PORT, HOST, function() {
    console.log("server up and running @ http://" + HOST + ":" + PORT);
});

/*CORS isn’t enabled on the server, this is due to security reasons by default,

so no one else but the webserver itself can make requests to the server.*/

app.use(function(req, res, next) {

    res.header("Access-Control-Allow-Origin", "*");
   
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    
    res.header("Access-Control-Allow-Methods","PUT,POST,PATCH,DELETE,GET");
    next();
   
});

module.exports = app;